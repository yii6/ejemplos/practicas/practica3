<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * Description of Usuarios
 * 
 * @author Marian
 */

class Usuarios extends Model{
    public $nombre;
    public $apellidos;
    public $edad;
    public $email;
    
    public function rules(){
        [['nombre', 'edad', 'email', 'required'],
            ['email', 'email']
            ];
    }
    
    public function attributeLabels(){
        return [
            'nombre' => 'Nombre de usuario',
        ];
    }
            
}

