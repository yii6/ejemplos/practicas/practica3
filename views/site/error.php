<?php

/** @var yii\web\View $this */
/** @var string $name */
/** @var $message string */
/** @var $exception Exception */

use yii\helpers\Html;

$this->title = $name;
?>
<div class="site-error">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="alert alert-danger">
        <?= nl2br(Html::encode($message)) ?>
    </div>

    <p>
        Ups.....
    </p>
    <p>
        Hay un problema
    </p>

</div>
