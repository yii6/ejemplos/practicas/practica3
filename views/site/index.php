<?php

/** @var yii\web\View $this */

$this->title = 'PRÁCTICA 1';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>PRÁCTICA 3</h1>

        <p class="lead">- En esta práctica vamos a trabajar con redireccionamientos básicos del framework y con formularios.
<br>- Vamos a realizar formularios utilizando Html, Html Helpers de Yii y con formularios activos utilizando modelos.
<br>- Además vamos a crear nuestros propios widgets.</p>
        
    </div>
</div>
